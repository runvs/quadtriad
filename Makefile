CXX = g++ -Wall -Wextra -O3 -std=c++11

all: obj/main.o LINK 

obj/time.o: src/Time.*
	$(CXX) -c src/Time.cpp -o obj/time.o

obj/positionconverter.o:  src/PositionConverter.* src/GameProperties.hpp
	$(CXX) -c src/PositionConverter.cpp -o obj/positionconverter.o

obj/random.o: src/Random.*
	$(CXX) -c src/Random.cpp -o obj/random.o

obj/game.o: src/Game.cpp
	$(CXX) -c -o obj/game.o src/Game.cpp

obj/gamestate.o: src/GameState.*
	$(CXX) -c -o obj/gamestate.o src/GameState.cpp 

obj/menustate.o: src/MenuState.* obj/gamestate.o src/Rules.hpp src/GameProperties.hpp
	$(CXX) -c -o obj/menustate.o src/MenuState.cpp

obj/playstate.o: src/PlayState.* obj/gamestate.o src/Rules.hpp src/GameProperties.hpp
	$(CXX) -c -o obj/playstate.o src/PlayState.cpp

obj/gameoverstate.o: src/GameOverState.* obj/gamestate.o src/GameProperties.hpp
	$(CXX) -c -o obj/gameoverstate.o src/GameOverState.cpp

obj/card.o: src/Card.* src/GameProperties.hpp
	$(CXX) -c -o obj/card.o src/Card.cpp

obj/slot.o: src/Slot.* src/GameProperties.hpp
	$(CXX) -c -o obj/slot.o src/Slot.cpp

obj/player.o: src/Player.* src/GameProperties.hpp
	$(CXX) -c -o obj/player.o src/Player.cpp

obj/main.o: src/main.cpp src/GameProperties.hpp
	$(CXX) -c -o obj/main.o src/main.cpp

LINK: obj/main.o obj/player.o obj/slot.o obj/card.o obj/playstate.o obj/menustate.o obj/gamestate.o obj/gameoverstate.o obj/game.o obj/time.o obj/positionconverter.o obj/random.o
	$(CXX) obj/main.o obj/player.o obj/slot.o obj/card.o obj/gameoverstate.o  obj/playstate.o obj/menustate.o obj/gamestate.o obj/game.o obj/time.o obj/positionconverter.o obj/random.o -o bin/quadtriad -lsfml-graphics -lsfml-window -lsfml-system



%o: %.cpp 
	$(CXX) -o $*.o -c $<

clean:
	rm obj/*

