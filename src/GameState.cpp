#include "GameState.hpp"
#include "Game.hpp"

GameState::GameState(std::shared_ptr<sf::RenderWindow> rw, Game* game):
    m_spRw(rw)
{
    m_spGame = game;
}
/*
GameState::~GameState()
{
    m_spRw.reset();
    m_spGame.reset();
}*/


void GameState::doDraw()
{
    m_spRw->clear();
    draw();
    m_spRw->display();

}
