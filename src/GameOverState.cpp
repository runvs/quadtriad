#include "GameOverState.hpp"
#include "MenuState.hpp"
#include "Game.hpp"
#include "GameProperties.hpp"
GameOverState::GameOverState(std::shared_ptr<sf::RenderWindow> rw, Game* game, bool p1Win) :
    GameState(rw, game), m_p1Win(p1Win)
{
    m_font.loadFromFile("data/lorimer.otf");
    m_text = sf::Text("", m_font);
}

void GameOverState::input()
{

}

void GameOverState::update()
{
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
        {
            m_spGame->switchState(std::make_shared<MenuState>(m_spRw, m_spGame));
        }
}

void GameOverState::draw()
{
    m_text.setCharacterSize(30);
    m_text.setString(m_p1Win ? "Player 1 won" : "Player 2 won");
    m_text.setColor((!m_p1Win)? GameProperties::GetBlue2() : GameProperties::GetRed2());
    m_text.setPosition(370, 300);
    m_spRw->draw(m_text);

    m_text.setCharacterSize(19);
    m_text.setColor( sf::Color(230, 230 ,230));
    m_text.setString("Press [Space] or [Return] to restart");
    m_text.setPosition(350,500);
    m_spRw->draw(m_text);
}

