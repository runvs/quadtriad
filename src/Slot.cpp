#include <iostream>
#include "Slot.hpp"
#include "GameProperties.hpp"
#include "PositionConverter.hpp"

Slot::Slot (int x, int y) : m_x (x), m_y(y)
{
    m_state= Slot::SlotState::SS_FREE;

    rshape.setSize(sf::Vector2f(GameProperties::Slot_SizeInPixel(), GameProperties::Slot_SizeInPixel()));
    rshape.setFillColor(sf::Color(200,200,200));
    rshape.setOutlineColor(sf::Color ( 128, 128, 128));
    rshape.setOutlineThickness(2);
    double px = PositionConverter::convertX(m_x);
    double py = PositionConverter::convertY(m_y);
    rshape.setPosition(px, py);

}

void Slot::draw(std::shared_ptr<sf::RenderWindow> rw)
{

    //std::cout << "Slot draw" << std::endl;
    
    rw->draw(rshape);

    if(m_pCard)
    {
        m_pCard->drawOnTable(rw, m_x, m_y);
    }
}

void Slot::update()
{
}

void Slot::input()
{
}

void Slot::placeCard(std::shared_ptr<Card> c, bool activePlayer)
{
    //std::cout << "place card (" << c->getUp() << " " << c->getDown() << ") at (" << m_x << " , " << m_y << ") for Player " << activePlayer << std::endl;  
    m_pCard = c;
    occupySlot(activePlayer);
}

void Slot::occupySlot(bool activePlayer)
{
    m_state = Slot::getStatusFromActivePlayer(activePlayer);
    if(activePlayer) 
    {
        rshape.setFillColor(GameProperties::GetRed1());
    }
    else 
    {
        rshape.setFillColor(GameProperties::GetBlue1());
    }
}

