#pragma once

#include <memory>
#include <SFML/Graphics.hpp>

// fwd decl
class Game;

class GameState
{
public:
    GameState(std::shared_ptr<sf::RenderWindow> rw, Game* game);
    
    void doDraw();

    //virtual ~GameState();
    virtual void input() = 0;
    virtual void update() = 0;

protected:
    std::shared_ptr<sf::RenderWindow> m_spRw;
    Game* m_spGame;
    
    virtual void draw() = 0;
};
