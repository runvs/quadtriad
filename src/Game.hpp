#pragma once
#include <memory>
#include <vector>

#include <SFML/Graphics.hpp>

class GameState;

class Game
{
public:
    void input();
    void update ();
    void draw();
    void init();

    Game (std::shared_ptr<sf::RenderWindow> rw);
    ~Game();

    void switchState(std::shared_ptr<GameState> newState);
private:
    std::shared_ptr<sf::RenderWindow> m_spRw;

    std::shared_ptr<GameState> m_spState;
};

