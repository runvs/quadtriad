#pragma once
#include <vector>
#include "GameState.hpp"

#include "Slot.hpp"
#include "Player.hpp"
#include "Rules.hpp"

class PlayState : public GameState
{
public:
    PlayState(std::shared_ptr<sf::RenderWindow> rw, Game* game, Rules r);

    void input();
    void update();
    void draw();

private:
    std::vector< Slot > m_table;
  
    std::vector<Slot*> m_check;
    std::vector<Slot*> m_checkNext;
    std::vector<Slot*> m_ignore;

    bool isSlotEmpty (int x, int y);

    void placeCard( int x, int y);


    double m_inputWallTime;

    int cursorX;
    int cursorY;

    sf::RectangleShape m_cursorShape;

    void checkCursorPosition();

    bool m_activePlayer;
    void switchPlayer();
    
    void checkTable();
    bool checkCards();

    Slot* getSlotOnPosition(const int tx, const int ty);

    bool m_isInCheckMode;
    double m_checkTimer;

    void endGame();

    Player m_player1;
    Player m_player2;
    Rules rules;
};
