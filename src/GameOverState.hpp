#pragma once 
#include "GameState.hpp"

class GameOverState : public GameState
{
public:
    GameOverState(std::shared_ptr<sf::RenderWindow> rw, Game* game, bool p1Win);

    void input();
    void update();
    void draw();

private:
    bool m_p1Win;
    
    sf::Font m_font;
    sf::Text m_text;
};
