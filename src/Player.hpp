#pragma once

#include <memory>
#include <vector>

#include <SFML/Graphics.hpp>

#include "Card.hpp"

class Player 
{
public:
    Player(bool p1);
    
    void getInput();

    void draw(std::shared_ptr<sf::RenderWindow> rw);
   
    std::shared_ptr<Card> RemoveCard ();  

    inline void setActive( bool val) {m_isActive = val;};

inline unsigned long GetNumberOfCards() const  {return m_cards.size();};

private:
    std::vector<std::shared_ptr<Card> > m_cards;

    sf::RectangleShape rshape;

    int m_selectedCardIndex;
    double m_inputWallTime;
    bool m_player1;
    bool m_isActive;
    void checkSelectedCard();
};
