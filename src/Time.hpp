#pragma once

class Time 
{
public:
    static void update (double t) 
    {
        s_realElapsed = t;
    };
    static double elapsedGameTime () {return Time::s_realElapsed * Time::s_factor;};

    static void setFactor ( double f) {Time::s_factor = f;};
private:
    static double s_realElapsed;
    static double s_factor;
};
