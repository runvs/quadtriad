#pragma once
#include "GameState.hpp"

class MenuState : public GameState
{
public:
    MenuState(std::shared_ptr<sf::RenderWindow> rw, Game* game);

    void input();
    void update();
    void draw();
private:

    inline void cycleDown() {selection++; if (selection> 2) selection = 0;};
    inline void cycleUp() {selection--; if (selection< 0) selection = 2;};
    void doAction ();

    int selection;
    double m_inputWallTime;

    sf::Font m_font;
    sf::Text m_text;

    bool same;
    bool combo;
};
