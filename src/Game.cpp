#include "Game.hpp"
#include "MenuState.hpp"
#include "PlayState.hpp"

Game::Game (std::shared_ptr<sf::RenderWindow> rw) 
{
    m_spRw = rw;
    //std::weak_ptr<Game> wptr(sptr);
}
void Game::init ()
{
    m_spState = std::make_shared<MenuState>(m_spRw, this);
}



Game::~Game () 
{
    //m_spRw.reset();
    //m_spState.reset();
}

void Game::input()
{
    if(m_spState) m_spState->input();
}

void Game::update()
{
    if(m_spState) m_spState->update();
}

void Game::draw()
{
    if(m_spState) m_spState->doDraw();
}

void Game::switchState( std::shared_ptr<GameState> newState)
{
    // reset the old state and replace it by the new state
    m_spState = newState;
}



