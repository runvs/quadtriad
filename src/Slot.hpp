#pragma once

#include <memory>
#include <SFML/Graphics.hpp>

#include "Card.hpp"

class Slot
{
public:
    Slot (int x, int y);
    Slot () = delete;

enum SlotState
{
    SS_FREE,
    SS_A,
    SS_B
};

    

    void draw(std::shared_ptr<sf::RenderWindow> rw);
    void update();
    void input();

    inline SlotState getState() {return m_state;};
    inline int getPosX () {return m_x;};    
    inline int getPosY () {return m_y;};    
       
    void placeCard(std::shared_ptr<Card> c, bool activePlayer);

    inline bool isEmpty() {return (m_state == SS_FREE);};

    void occupySlot(bool activePlayer);

    std::shared_ptr<Card> GetCard() {return m_pCard;};

    static SlotState getStatusFromActivePlayer(bool active)
    {
        if (active)
            return Slot::SlotState::SS_A;
        
        return Slot::SlotState::SS_B;
    }

private:
    int m_x;
    int m_y;
    SlotState m_state;


    std::shared_ptr<Card> m_pCard;
    sf::RectangleShape rshape;
};
