#include <iostream>
#include <memory>
#include "MenuState.hpp"
#include "PlayState.hpp"
#include "Game.hpp"
#include "Time.hpp"
#include "GameProperties.hpp"
#include "Rules.hpp"

MenuState::MenuState(std::shared_ptr<sf::RenderWindow> rw, Game* game)
    :GameState(rw, game), selection(0), m_inputWallTime(0.25), same(false), combo(false)
{
    m_font.loadFromFile("data/lorimer.otf");
    m_text = sf::Text("", m_font);
}

void MenuState::input()
{
    if(m_inputWallTime <= 0)
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            cycleDown();
            m_inputWallTime += GameProperties::Input_WallTime();
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            cycleUp();
            m_inputWallTime += GameProperties::Input_WallTime();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right)  ||  sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
        {
            m_inputWallTime += GameProperties::Input_WallTime();
            doAction();
        }
    }
}

void MenuState::doAction()
{
    if( selection == 0)
    {
        same = !same;
    }
    else if (selection == 1)
    {
        combo = !combo;
    }
    else if(selection == 2)
    {
        if(m_spGame != nullptr)
        {
            Rules r(same, combo);
            m_spGame->switchState(std::make_shared<PlayState>(m_spRw, m_spGame, r));
        }
    }
}
void MenuState::update()
{
    if(m_inputWallTime > 0)
    {
        m_inputWallTime -= Time::elapsedGameTime();
    }
}

void MenuState::draw()
{
    m_text.setColor( sf::Color(255,255 ,255));
    m_text.setString("QUAD TRIAD");
    m_text.setCharacterSize(50);
    m_text.setPosition(340,50);
    m_spRw->draw(m_text);

    double xoffs = 350;

    m_text.setCharacterSize(30);
    m_text.setString("Same");
    m_text.setColor((selection!=0)?sf::Color(125,125,125): sf::Color(220, 240, 240)   );
    m_text.setPosition(xoffs, 230);
    m_spRw->draw(m_text);
    m_text.setString(same ?"yes" : "no");
    m_text.setColor((selection!=0)?sf::Color(125,125,125): sf::Color(220, 240, 240)   );
    m_text.setPosition(xoffs + 100, 230);
    m_spRw->draw(m_text);

    m_text.setString("Combo");
    m_text.setColor((selection!=1)?sf::Color(125,125,125): sf::Color(220, 240, 240)   );
    m_text.setPosition(xoffs, 260);
    m_spRw->draw(m_text);
    m_text.setString(combo ?"yes" : "no");
    m_text.setColor((selection!=1)?sf::Color(125,125,125): sf::Color(220, 240, 240)   );
    m_text.setPosition(xoffs + 100, 260);
    m_spRw->draw(m_text);

    m_text.setString("Play");
    m_text.setColor((selection!=2)?sf::Color(125,125,125): sf::Color(220, 240 ,240)   );
    m_text.setPosition(xoffs + 50, 290);
    m_spRw->draw(m_text);


    m_text.setCharacterSize(25);
    m_text.setColor( sf::Color(125, 125, 125));
    m_text.setString("Controls:\n[Arrows] to move\n[Space] to place a card\n[Q,A] to switch Cards\n[Return to Start]");
    m_text.setPosition(200,360);
    m_spRw->draw(m_text);

    m_text.setCharacterSize(19);
    m_text.setColor( sf::Color(125, 125, 125));
    m_text.setString("a game by Simon Weis (@laguna_999)\ncheck out http://runvs.io");
    m_text.setPosition(450,465);
    m_spRw->draw(m_text);
}

