#pragma once

#include <random>

class Random
{
    private:
        static std::mt19937 gen;
        static std::normal_distribution<> normal_dist;
    public:

        static int cV();
};
