#pragma once
#include <SFML/Graphics.hpp>

class GameProperties
{
public:
    static constexpr unsigned int Window_Width() { return 800;};
    static constexpr unsigned int Window_Height() {return 600;};

    static constexpr double Input_WallTime() {return 0.25;};

    static constexpr unsigned int Table_Size() {return 3;};
    static constexpr double Table_CheckTimer() {return 0.35;};

    static constexpr double Slot_SizeInPixel() {return 150;};
    static constexpr double Slot_OffsetTotalX() {return 175;};
    static constexpr double Slot_OffsetTotalY() {return 40;};
    static constexpr double Slot_Margin()  {return 8;};

    static constexpr double Card_UpDownOffsetX() {return 70;};
    static constexpr double Card_UpOffsetY() {return 3;};
    static constexpr double Card_DownOffsetY() {return 110;};

    static constexpr double Card_LeftRightOffsetY() {return 55;};
    static constexpr double Card_LeftOffsetX() {return 5;};
    static constexpr double Card_RightOffsetX() {return 125;};

    static constexpr double Player_CardsOffsetX1() {return 10;};
    static constexpr double Player_CardsOffsetX2 () {return 640;};
    static constexpr double Player_CardsOffsetY () {return 10;};
    static constexpr double Player_CardsHeightTotal() {return 500;};

    static sf::Color GetGray1 () {return sf::Color(200,200,200);};
    static sf::Color GetGray2 () {return sf::Color(128,128,128);};

    static sf::Color GetBlue1 () {return sf::Color(10, 10, 120);};
    static sf::Color GetBlue2 () {return sf::Color(30, 30, 170);};

    static sf::Color GetRed1 () {return sf::Color(120, 10, 10);};
    static sf::Color GetRed2 () {return sf::Color(170, 30, 30);};

    static sf::Color GetTransparent () {return sf::Color(255, 255, 255, 0);};
};


#include <sstream>

template <typename T>
static std::string NumberToString ( T Number )
{
    std::ostringstream ss;
    ss << Number;
    return ss.str();
}



