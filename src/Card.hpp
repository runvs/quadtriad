#pragma once
#include <memory>
#include <SFML/Graphics.hpp>
class Card 
{
public:
  
    Card (unsigned int up, unsigned int down, unsigned int left, unsigned int right);

    inline unsigned int getUp() {return u;};
    inline unsigned int getDown() {return d;};
    inline unsigned int getLeft() {return l;};
    inline unsigned int getRight() {return r;};

    void update();
    void drawOnTable(std::shared_ptr<sf::RenderWindow> rw, double posX, double posY);
    void drawOnStack(std::shared_ptr<sf::RenderWindow> rw, double posX, double posY);
    
    // dx, dy name the position of the other card, relative to this one
    bool compare (std::shared_ptr<Card> other, int dx, int dy, const bool s);    


private:

    sf::Font m_font;
    sf::Text m_text;

    unsigned int u;
    unsigned int d;
    unsigned int l;
    unsigned int r;
    

};
