#include <memory>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "Game.hpp"
#include "Time.hpp"
#include "GameProperties.hpp"
int main()
{

    std::shared_ptr<sf::RenderWindow> window = std::make_shared<sf::RenderWindow>(
            sf::VideoMode(GameProperties::Window_Width(),GameProperties::Window_Height()), "Quad triad");
    Game* g =new Game(window);

    g->init();
    sf::Clock clock;

    window->setFramerateLimit(60);

    double frameDuration = 0.016;

    while (window->isOpen())
    {
        frameDuration = clock.restart().asSeconds();
        Time::update(frameDuration);

        g->input();
        g->update();
        g->draw();

        sf::Event event;
        while (window->pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window->close();
        }
    }
    delete g;

    return 0;
}
