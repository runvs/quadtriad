#include "PositionConverter.hpp"

#include "GameProperties.hpp"

double PositionConverter::convertX(int x)
{
    return  GameProperties::Slot_OffsetTotalX() + (GameProperties::Slot_Margin() + GameProperties::Slot_SizeInPixel()) * x;
}
double PositionConverter::convertY(int y)
{
    return GameProperties::Slot_OffsetTotalY() + (GameProperties::Slot_Margin() + GameProperties::Slot_SizeInPixel()) * y;
}
