#pragma once

struct PositionConverter
{
    static double convertX (int x);
    static double convertY (int y);
};
