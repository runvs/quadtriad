#include <cmath>
#include <iostream>
#include "Game.hpp"
#include "Time.hpp"
#include "PlayState.hpp"
#include "GameOverState.hpp"
#include "GameProperties.hpp"
#include "PositionConverter.hpp"

PlayState::PlayState(std::shared_ptr<sf::RenderWindow> rw, Game* game, Rules r):
    GameState(rw, game), m_player1(Player(true)), m_player2(Player(false)), rules (r)
{
    m_activePlayer = false;

    m_inputWallTime = 0.25;
    cursorX = 1;
    cursorY = 1;

    m_isInCheckMode = false;
    m_checkTimer = 0.0;

    for (int i = 0; i != 3; ++i)
    for (int j = 0; j != 3; ++j)
    {
        m_table.push_back( Slot(i, j));
    }

    m_cursorShape.setSize(sf::Vector2f(GameProperties::Slot_SizeInPixel(), GameProperties::Slot_SizeInPixel()));
    m_cursorShape.setFillColor(GameProperties::GetTransparent());
    m_cursorShape.setOutlineColor(GameProperties::GetBlue2());
    m_cursorShape.setOutlineThickness(5);
}

void PlayState::input()
{
    m_player2.getInput();
    m_player1.getInput();

    if (m_inputWallTime >0 )
    {
    }
    else
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            m_inputWallTime += GameProperties::Input_WallTime();
            cursorX--;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            m_inputWallTime += GameProperties::Input_WallTime();
            cursorX++;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            m_inputWallTime += GameProperties::Input_WallTime();
            cursorY--;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            m_inputWallTime += GameProperties::Input_WallTime();
            cursorY++;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
        {
            m_inputWallTime += GameProperties::Input_WallTime();
            placeCard( cursorX, cursorY);
        }
    }
}

void PlayState::update()
{
    if(!m_isInCheckMode)
    {
        if (m_inputWallTime > 0)
        {
            m_inputWallTime -= Time::elapsedGameTime();
        }

        //std::cout << cursorX << " " <<  cursorY << std::endl;
        checkCursorPosition();
        checkTable();

        m_player1.setActive(m_activePlayer);
        m_player2.setActive(!m_activePlayer);

    }
    else
    {
        m_checkTimer -= Time::elapsedGameTime();
        //std::cout << m_checkTimer << std::endl;
        if(m_checkTimer <= 0)
        {
            if(checkCards())
            {
                m_checkTimer = GameProperties::Table_CheckTimer();
            }
            else
            {
                m_isInCheckMode = false;
                switchPlayer();
            }
        }
    }
}

void PlayState::draw()
{
    for (auto it = m_table.begin(); it != m_table.end(); ++it)
    {
        (*it).draw(m_spRw);
    }
    m_spRw->draw(m_cursorShape);


    m_player1.draw(m_spRw);
    m_player2.draw(m_spRw);

}


bool PlayState::isSlotEmpty (int x, int y)
{
    int idx = x * 3 + y;
    return m_table.at(idx).isEmpty();
}

void PlayState::placeCard( int x, int y)
{
    if (isSlotEmpty(x,y))
    {
        std::shared_ptr<Card> c = (m_activePlayer) ? m_player1.RemoveCard() : m_player2.RemoveCard();
        int idx = x*3+y;
        m_table.at(idx).placeCard(c, m_activePlayer);
        m_checkNext.push_back(&m_table.at(idx));
        m_ignore.clear();
        m_ignore.push_back(&m_table.at(idx));
        if(!rules.combo)
        {
            std::vector<std::pair<int,int> > pos;
            pos.push_back(std::make_pair(-1,-1));
            pos.push_back(std::make_pair(1,-1 ));
            pos.push_back(std::make_pair(-1,1));
            pos.push_back(std::make_pair(1,1 ));

            pos.push_back(std::make_pair(-2,0));
            pos.push_back(std::make_pair(2,0));
            pos.push_back(std::make_pair(0,-2));
            pos.push_back(std::make_pair(0,2 ));

            pos.push_back(std::make_pair(-2,-1));
            pos.push_back(std::make_pair(2,-1));
            pos.push_back(std::make_pair(-2,1));
            pos.push_back(std::make_pair(2,1));

            pos.push_back(std::make_pair(1,-2));
            pos.push_back(std::make_pair(1,2 ));
            pos.push_back(std::make_pair(-1,-2));
            pos.push_back(std::make_pair(-1,2 ));

            pos.push_back(std::make_pair(-2,-2));
            pos.push_back(std::make_pair(2,-2 ));
            pos.push_back(std::make_pair(-2,2));
            pos.push_back(std::make_pair(2,2 ));
            for (auto itPos = pos.begin(); itPos != pos.end(); ++itPos)
            {
                int dx = (*itPos).first;
                int dy = (*itPos).second;
                //std::cout << "\t with " << x+dx << " " << y +dy << std::endl;
                Slot* other = getSlotOnPosition(x+dx, y + dy);
                if (other != nullptr)
                {
                    m_ignore.push_back(other);
                }
            }
        }
        m_isInCheckMode = true;
        m_checkTimer = GameProperties::Table_CheckTimer();

    }
}

void PlayState::checkCursorPosition()
{
    if (cursorX < 0)
    {
        cursorX = 0;
    }
    if (static_cast<unsigned int> (cursorX) > GameProperties::Table_Size()-1)
    {
        cursorX = GameProperties::Table_Size() -1;
    }
    if (cursorY < 0)
    {
        cursorY = 0;
    }
    if (static_cast<unsigned int> (cursorY) > GameProperties::Table_Size()-1)
    {
        cursorY = GameProperties::Table_Size() -1;
    }

    double px = PositionConverter::convertX(cursorX);
    double py = PositionConverter::convertY(cursorY);
    m_cursorShape.setPosition(px, py);
}


void PlayState::switchPlayer()
{
    //std::cout << "old " << m_activePlayer << std::endl;
    m_activePlayer = !m_activePlayer;
    //std::cout << "new " << m_activePlayer << std::endl;

    if(m_activePlayer)
    {
        m_cursorShape.setOutlineColor (GameProperties::GetRed2());
    }
    else
    {
        m_cursorShape.setOutlineColor (GameProperties::GetBlue2());
    }
}

void PlayState::checkTable()
{
    unsigned int numberOfPlacedCards = 0;
    for (auto it = m_table.begin(); it != m_table.end(); ++it)
    {
        if((*it).getState() != Slot::SlotState::SS_FREE)
        {
            numberOfPlacedCards++;
        }
    }
    if(numberOfPlacedCards >= m_table.size())
    {
        endGame();
    }
}


bool PlayState::checkCards()
{
    //std::cout << "----------------------" << std::endl;
    bool retval = false;
    m_check = m_checkNext;
    m_checkNext.clear();
    for (auto it = m_check.begin(); it != m_check.end(); ++it)
    {
        int x = (*it)->getPosX();
        int y = (*it)->getPosY();
        Slot::SlotState myState = (*it)->getState();
        if(myState == Slot::SlotState::SS_FREE)
        {
            continue;
        }
        Slot* pSlot = getSlotOnPosition(x,y);
        //std::cout << "checking " << x << " " << y << std::endl;
        if ( pSlot != nullptr)
        {
            //std::cout << "true check" << std::endl;
            std::vector<std::pair<int,int> > pos;
            pos.push_back(std::make_pair(-1,0));
            pos.push_back(std::make_pair(1,0));
            pos.push_back(std::make_pair(0,-1));
            pos.push_back(std::make_pair(0,1 ));
            for (auto itPos = pos.begin(); itPos != pos.end(); ++itPos)
            {
                int dx = (*itPos).first;
                int dy = (*itPos).second;
                //std::cout << "\t with " << x+dx << " " << y +dy << std::endl;
                Slot* other = getSlotOnPosition(x+dx, y + dy);
                if (other != nullptr)
                {
                    bool ignore = false;
                    for (auto it2 = m_ignore.begin(); it2 != m_ignore.end(); ++it2)
                    {
                        if(other == *it2)
                        {
                            ignore = true;
                            break;
                        }
                    }
                    if (ignore) continue;
                    if(other->getState() != Slot::SlotState::SS_FREE && other->getState() != myState )
                    {
                        std::shared_ptr<Card> thisCard = (*it)->GetCard();
                        std::shared_ptr<Card> otherCard = other->GetCard();
                        if(thisCard->compare(otherCard, dx,dy,rules.same))
                        {
                            //std::cout << "\t\tflip " << std::endl;
                            other->occupySlot(m_activePlayer);
                            m_checkNext.push_back(other);
                            retval = true;
                        }
                    }
                }
            }
        }
        //m_ignore.push_back(*it);
    }
    return retval;
}


Slot* PlayState::getSlotOnPosition(const int tx, const int ty)
{
    Slot* pRet = nullptr;

    if (tx >= 0 && static_cast<unsigned int>(tx) <= GameProperties::Table_Size() -1 )
    if (ty >= 0 && static_cast<unsigned int>(ty) <= GameProperties::Table_Size() -1 )
    {
        for (auto it = m_table.begin(); it != m_table.end(); ++it)
        {
            if ((*it).getPosX() == tx && (*it).getPosY() == ty)
            {
                pRet = &(*it);
                break;
            }
        }
    }

    return pRet;
}

void PlayState::endGame()
{
    // determine winner:
    int p1Cards = m_player1.GetNumberOfCards();
    int p2Cards = m_player2.GetNumberOfCards();

    for (auto it = m_table.begin(); it != m_table.end(); ++it)
    {
        if ((*it).getState() == Slot::SlotState::SS_A)
        {
            p1Cards++;
        }
        else if ((*it).getState() == Slot::SlotState::SS_B)
        {
            p2Cards++;
        }
        else
        {
            throw std::string ("logic error: game over and not all cards placed");
        }
    }

    bool p1Wins = false;
    if (p1Cards> p2Cards)
    {
        p1Wins = true;
    }
    else if (p2Cards> p1Cards)
    {
        //std::cout << "p2 Wins" << std::endl;
    }
    else
    {
        //std::cout << "draw" << std::endl;
    }

    m_spGame->switchState(std::make_shared<GameOverState> (m_spRw, m_spGame, p1Wins));
}
