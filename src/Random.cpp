#include "Random.hpp"
#include <time.h>
std::mt19937 Random::gen(time(0));
std::normal_distribution<> Random::normal_dist(4,3);


int Random::cV()
{
    int n = normal_dist(gen);
    if(n >= 1 && n <= 10)
    {
        return n;
    }
    else
    {
        return Random::cV();
    }

}
