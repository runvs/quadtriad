#include <memory>
#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics/Font.hpp>
#include "Card.hpp"
#include "GameProperties.hpp"
#include "PositionConverter.hpp"

Card::Card (unsigned int up, unsigned int down, unsigned int left, unsigned int right)
{
    u = up;
    l = left;
    d = down;
    r=right;

    m_font.loadFromFile("data/lorimer.otf");
    m_text = sf::Text("", m_font);
}


void Card::update()
{

}

void Card::drawOnStack(std::shared_ptr<sf::RenderWindow> rw, double posX, double posY)
{
    //std::cout <<  "dwar card stack at " << posX << " " << posY << std::endl;
    m_text.setString(NumberToString(u));
    m_text.setPosition(
            posX + GameProperties::Card_UpDownOffsetX() ,
            posY + GameProperties::Card_UpOffsetY());
    rw->draw(m_text);

    m_text.setString(NumberToString(d));
    m_text.setPosition(
            posX + GameProperties::Card_UpDownOffsetX() ,
            posY + GameProperties::Card_DownOffsetY());
    rw->draw(m_text);

    m_text.setString(NumberToString(l));
    m_text.setPosition(
            posX + GameProperties::Card_LeftOffsetX() ,
            posY + GameProperties::Card_LeftRightOffsetY());
    rw->draw(m_text);

    m_text.setString(NumberToString(r));
    m_text.setPosition(
            posX + GameProperties::Card_RightOffsetX() ,
            posY + GameProperties::Card_LeftRightOffsetY());
    rw->draw(m_text);


    //std::cout << "card draw" << std::endl;
}
void Card::drawOnTable(std::shared_ptr<sf::RenderWindow> rw, double posX, double posY)
{
    m_text.setString(NumberToString(u));
    m_text.setPosition(
            PositionConverter::convertX(posX) + GameProperties::Card_UpDownOffsetX() ,
            PositionConverter::convertY(posY) + GameProperties::Card_UpOffsetY());
    rw->draw(m_text);

    m_text.setString(NumberToString(d));
    m_text.setPosition(
            PositionConverter::convertX(posX) + GameProperties::Card_UpDownOffsetX() ,
            PositionConverter::convertY(posY) + GameProperties::Card_DownOffsetY());
    rw->draw(m_text);

    m_text.setString(NumberToString(l));
    m_text.setPosition(
            PositionConverter::convertX(posX) + GameProperties::Card_LeftOffsetX() ,
            PositionConverter::convertY(posY) + GameProperties::Card_LeftRightOffsetY());
    rw->draw(m_text);

    m_text.setString(NumberToString(r));
    m_text.setPosition(
            PositionConverter::convertX(posX) + GameProperties::Card_RightOffsetX() ,
            PositionConverter::convertY(posY) + GameProperties::Card_LeftRightOffsetY());
    rw->draw(m_text);


    //std::cout << "card draw" << std::endl;
}



bool Card::compare (std::shared_ptr<Card> other, int dx, int dy, const bool s)
{
    int myval = 0;
    int otherval = 0;
    if (dx < 0)
    {
        myval = getLeft();
        otherval = other->getRight();
    }
    else if (dx > 0)
    {
        myval = getRight();
        otherval = other->getLeft();
    }
    else
    {
        if (dy < 0)
        {
            myval = getUp();
            otherval = other->getDown();
        }
        else
        {
            myval = getDown();
            otherval = other->getUp();
        }
    }

    //std::cout <<" compare "  <<myval << " " << otherval << std::endl;
    bool retval = false;
    if( otherval < myval + (s ? 1 : 0) )
    {
        retval = true;
    }
    return retval;
}

