#include <iostream>
#include "Player.hpp"

#include "GameProperties.hpp"
#include "Random.hpp"
#include "Time.hpp"

Player::Player(bool p1)
{
    for (int i = 0; i != 5; ++i)
    {
        std::shared_ptr<Card> sp_C = std::make_shared<Card>(Random::cV(),Random::cV(),Random::cV(),Random::cV());
        
        m_cards.push_back(sp_C);
    }

    m_selectedCardIndex = 1;
    m_inputWallTime = 0;


    m_isActive = false;
    m_player1 = p1;
    
    rshape.setSize(sf::Vector2f(GameProperties::Slot_SizeInPixel(), GameProperties::Slot_SizeInPixel()));
    rshape.setFillColor((p1)?GameProperties::GetRed1() : GameProperties::GetBlue1());
    rshape.setOutlineColor((p1)?GameProperties::GetRed2() : GameProperties::GetBlue2());
    rshape.setOutlineThickness(2);
}

void Player::getInput()
{
    if(m_isActive)
    {
        if(m_inputWallTime > 0)
        {
            m_inputWallTime -= Time::elapsedGameTime();
        }
        else
        {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
            {
                m_inputWallTime += GameProperties::Input_WallTime();
                m_selectedCardIndex--;
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
            {
        
                m_inputWallTime += GameProperties::Input_WallTime();
                m_selectedCardIndex++;
            }
        }
    }

    checkSelectedCard();
}


void Player::checkSelectedCard()
{
    if (m_selectedCardIndex <0)
    {
        m_selectedCardIndex = m_cards.size()-1;
    }
    else if (static_cast<unsigned int>(m_selectedCardIndex) >= m_cards.size())
    {
        m_selectedCardIndex = 0;
    }
}

void Player::draw(std::shared_ptr<sf::RenderWindow> rw)
{
    double yspacing = GameProperties::Player_CardsHeightTotal() / m_cards.size();
    //std::cout << m_cards.size() << std::endl;
    for(unsigned int i = 0; i != m_cards.size(); ++i) 
    {
        if ( !m_isActive || i != static_cast<unsigned int>(m_selectedCardIndex))
        {
            double px = (m_player1) ? GameProperties::Player_CardsOffsetX1() : GameProperties::Player_CardsOffsetX2();
            double py = GameProperties::Player_CardsOffsetY() + yspacing * i;
            rshape.setPosition(px, py);
            rw->draw(rshape);
            m_cards.at(i)->drawOnStack(rw, px, py); 
        }

    } 
    
    // draw selected card last
    if(m_isActive && m_cards.size() != 0)
    {
        double px = (m_player1) ? GameProperties::Player_CardsOffsetX1() + 40: GameProperties::Player_CardsOffsetX2() - 40;
        double py = GameProperties::Player_CardsOffsetY() + yspacing * m_selectedCardIndex;
        rshape.setPosition(px, py);
        rw->draw(rshape);
        m_cards.at(m_selectedCardIndex)->drawOnStack(rw, px, py);
    }
} 

std::shared_ptr<Card> Player::RemoveCard()
{
    //std::cout << m_cards.size() << " " << m_selectedCardIndex << std::endl;
    auto it = m_cards.begin() + m_selectedCardIndex;
    
    std::shared_ptr<Card> retval = (*it);

    m_cards.erase(it);
    checkSelectedCard();
    return retval;
}
